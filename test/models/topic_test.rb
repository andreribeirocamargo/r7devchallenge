require 'test_helper'

class TopicTest < ActiveSupport::TestCase
  test "pagination page zero" do
    current_page, total_pages, topics = Topic.paginate(
      page: 0,
      batch_size: 1,
      records_count: 1,
      sorting_algorithm: [{ id: :desc }, { id: :asc }]
    )
    assert_equal 1, current_page
    assert_equal 1, total_pages
    assert_equal 0, topics.options[:skip]
    assert_equal 1, topics.options[:limit]
    assert_equal({ "_id" => -1 }, topics.options[:sort])
  end

  test "pagination first page" do
    current_page, total_pages, topics = Topic.paginate(
      page: 1,
      batch_size: 1,
      records_count: 1,
      sorting_algorithm: [{ id: :desc },{ id: :asc }]
    )
    assert_equal 1, current_page
    assert_equal 1, total_pages
    assert_equal 0, topics.options[:skip]
    assert_equal 1, topics.options[:limit]
    assert_equal({ "_id" => -1 }, topics.options[:sort])

    current_page, total_pages, topics = Topic.paginate(
      page: 1,
      batch_size: 1,
      records_count: 10,
      sorting_algorithm: [{ id: :desc },{ id: :asc }]
    )
    assert_equal 1, current_page
    assert_equal 10, total_pages
    assert_equal 0, topics.options[:skip]
    assert_equal 1, topics.options[:limit]
    assert_equal({ "_id" => -1 }, topics.options[:sort])
  end

  test "pagination second page" do
    current_page, total_pages, topics = Topic.paginate(
      page: 2,
      batch_size: 10,
      records_count: 1000,
      sorting_algorithm: [{ id: :desc },{ id: :asc }]
    )
    assert_equal 2, current_page
    assert_equal 100, total_pages
    assert_equal 10, topics.options[:skip]
    assert_equal 10, topics.options[:limit]
    assert_equal({ "_id" => -1 }, topics.options[:sort])
  end

  test "evenly pagination middle page" do
    current_page, total_pages, topics = Topic.paginate(
      page: 2,
      batch_size: 1,
      records_count: 4,
      sorting_algorithm: [{ id: :desc },{ id: :asc }]
    )
    assert_equal 2, current_page
    assert_equal 4, total_pages
    assert_equal 1, topics.options[:skip]
    assert_equal 1, topics.options[:limit]
    assert_equal({ "_id" => -1 }, topics.options[:sort])
  end

  test "oddly pagination middle page" do
    current_page, total_pages, topics = Topic.paginate(
      page: 3,
      batch_size: 1,
      records_count: 5,
      sorting_algorithm: [{ id: :desc },{ id: :asc }]
    )
    assert_equal 3, current_page
    assert_equal 5, total_pages
    assert_equal 2, topics.options[:skip]
    assert_equal 1, topics.options[:limit]
    assert_equal({ "_id" => -1 }, topics.options[:sort])
  end

  test "pagination last page" do
    current_page, total_pages, topics = Topic.paginate(
      page: 100,
      batch_size: 10,
      records_count: 1000,
      sorting_algorithm: [{ id: :desc },{ id: :asc }]
    )
    assert_equal 100, current_page
    assert_equal 100, total_pages
    assert_equal 0, topics.options[:skip]
    assert_equal 10, topics.options[:limit]
    assert_equal({ "_id" => 1 }, topics.options[:sort])
  end

  test "pagination second to last page" do
    current_page, total_pages, topics = Topic.paginate(
      page: 99,
      batch_size: 10,
      records_count: 1000,
      sorting_algorithm: [{ id: :desc },{ id: :asc }]
    )
    assert_equal 99, current_page
    assert_equal 100, total_pages
    assert_equal 10, topics.options[:skip]
    assert_equal 10, topics.options[:limit]
    assert_equal({ "_id" => 1 }, topics.options[:sort])
  end
end
