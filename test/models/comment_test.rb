require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "blacklist shuffle" do
    input_words_array = %w{arma carro}
    input_variations_hash = {
      'a'=>[ '4', '@'],
      'c'=>[ 'k' ],
      'o'=>[ '0' ]
    }
    output_array_regexps = [
      /a\s*r\s*m\s*a/mi,
      /a\s*r\s*m\s*4/mi,
      /a\s*r\s*m\s*@/mi,
      /4\s*r\s*m\s*a/mi,
      /4\s*r\s*m\s*4/mi,
      /4\s*r\s*m\s*@/mi,
      /@\s*r\s*m\s*a/mi,
      /@\s*r\s*m\s*4/mi,
      /@\s*r\s*m\s*@/mi,
      /c\s*a\s*r\s*r\s*o/mi,
      /c\s*a\s*r\s*r\s*0/mi,
      /c\s*4\s*r\s*r\s*o/mi,
      /c\s*4\s*r\s*r\s*0/mi,
      /c\s*@\s*r\s*r\s*o/mi,
      /c\s*@\s*r\s*r\s*0/mi,
      /k\s*a\s*r\s*r\s*o/mi,
      /k\s*a\s*r\s*r\s*0/mi,
      /k\s*4\s*r\s*r\s*o/mi,
      /k\s*4\s*r\s*r\s*0/mi,
      /k\s*@\s*r\s*r\s*o/mi,
      /k\s*@\s*r\s*r\s*0/mi
    ]
    assert_equal output_array_regexps, Comment.blacklist(input_words_array, input_variations_hash)
  end

  test "topic integrity" do
    topic1 = Topic.create(name: 'This is the topic one')
    topic2 = Topic.create(name: 'This is the topic two')
    topic1_comment1 = Comment.create(topic: topic1, message: 'This is a commentary on topic1')
    topic2_comment1 = Comment.create(topic: topic2, message: 'This is a commentary on topic2')
    twisted_commentary = Comment.new(topic: topic2, message: 'This is an attempt to break integrity', parent: topic1_comment1)
    assert twisted_commentary.invalid?
    assert_equal [I18n.t('errors.messages.invalid')], twisted_commentary.errors[:parent]
  end

  test "parent attribution" do
    topic = Topic.new(name: 'This is a topic')
    assert topic.save
    comment1 = Comment.new(topic: topic, message: 'This is a comment')
    assert comment1.save
    assert_equal nil, comment1.parent
    assert_equal comment1.id.to_s, comment1.path
    assert_equal 1, comment1.depth
    comment2 = Comment.new(topic: topic, message: 'This is a reply of the comment')
    comment2.parent = comment1
    assert comment2.save
    assert_equal comment1, comment2.parent
    assert_equal "#{comment1.id},#{comment2.id}", comment2.path
    assert_equal 2, comment2.depth
    comment3 = Comment.new(topic: topic, message: 'This is a reply of the reply of the comment')
    comment3.parent_id = comment2.id
    assert comment3.save
    assert_equal comment2, comment3.parent
    assert_equal "#{comment1.id},#{comment2.id},#{comment3.id}", comment3.path
    assert_equal 3, comment3.depth
  end

  test "replace bad words" do
    [
      [ 'ARMA',
        'xxxx' ],
      [ 'Carro',
        'xxxxx' ],
      [ "Carro\n",
        "xxxxx\n" ],
      [ 'Arma arma ARMA',
        'xxxx xxxx xxxx' ],
      [ 'Carro carro CARRO',
        'xxxxx xxxxx xxxxx' ],
      [ 'palavra ARMA não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra arma não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra 4rma não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra 4rm4 não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra 4rm@ não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra @rma não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra @rm4 não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra @rm@ não é permitida',
        'palavra xxxx não é permitida' ],
      [ 'palavra carro não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra c4rro não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra c4rr0 não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra c@rro não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra c@rr0 não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra karro não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra k4rro não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra k4rr0 não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra k@rro não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra k@rr0 não é permitida',
        'palavra xxxxx não é permitida' ],
      [ 'palavra c a r r o não é permitida',
        'palavra xxxxxxxxx não é permitida' ],
      [ 'palavra c  a  rr o não é permitida',
        'palavra xxxxxxxxxx não é permitida' ],
      [ 'c  a  rr o não é permitida',
        'xxxxxxxxxx não é permitida' ],
      [ 'não é permitida c  a  rr o',
        'não é permitida xxxxxxxxxx' ],
      [ "as palavras:\n\nArma e Carro\n\nnão são permitidas",
        "as palavras:\n\nxxxx e xxxxx\n\nnão são permitidas" ],
      [ "álcool no trânsito: não transforme seu carro em uma arma!",
        "álcool no trânsito: não transforme seu xxxxx em uma xxxx!" ]
    ].each do |input, output|
      assert_equal output, Comment.replace_bad_words(input)
    end
  end

  test "good words" do
    [
      'Lei do desarmamento está tramitando no congresso',
      'A carroceria do veículo é de alta resistência',
      'Vende-se uma armadura medieval seminova.'
    ].each do |input|
      assert_equal input, Comment.replace_bad_words(input)
    end
  end
end
