require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  include ApplicationHelper

  setup do
    Rails.application.routes.default_url_options[:controller] = 'topics'
  end

  test "paginator empty" do
    assert_equal "", paginator(-1, -1)
    assert_equal "", paginator(-1,  0)
    assert_equal "", paginator( 0, -1)
    assert_equal "", paginator( 0,  0)
  end

  test "paginator one page" do
    assert_equal "", paginator(1, 1)
  end

  test "paginator current_page greater than total_pages" do
    assert_equal %{<a href="/?p=1">anterior</a> | <a href="/?p=1">1</a> | 2}, paginator(22, 2)
  end

  test "paginator two pages" do
    assert_equal %{1 | <a href="/?p=2">2</a> | <a href="/?p=2">próxima</a>}, paginator(1, 2)
    assert_equal %{<a href="/?p=1">anterior</a> | <a href="/?p=1">1</a> | 2}, paginator(2, 2)
  end

  test "paginator three pages" do
    assert_equal %{1 | <a href="/?p=2">2</a> | <a href="/?p=3">3</a> | <a href="/?p=2">próxima</a>}, paginator(1, 3)
    assert_equal %{<a href="/?p=1">anterior</a> | <a href="/?p=1">1</a> | 2 | <a href="/?p=3">3</a> | <a href="/?p=3">próxima</a>}, paginator(2, 3)
  end

  test "paginator four pages" do
    assert_equal %{1 | <a href="/?p=2">2</a> | <a href="/?p=3">3</a> | <a href="/?p=4">4</a> | <a href="/?p=2">próxima</a>}, paginator(1, 4)
  end

  test "paginator five pages" do
    assert_equal %{1 | <a href="/?p=2">2</a> | <a href="/?p=3">3</a> | <a href="/?p=4">4</a> | <a href="/?p=5">5</a> | <a href="/?p=2">próxima</a>}, paginator(1, 5)
    assert_equal %{<a href="/?p=3">anterior</a> | <a href="/?p=1">1</a> | <a href="/?p=2">2</a> | <a href="/?p=3">3</a> | 4 | <a href="/?p=5">5</a> | <a href="/?p=5">próxima</a>}, paginator(4, 5)
  end

  test "paginator six pages" do
    assert_equal %{1 | <a href="/?p=2">2</a> | <a href="/?p=3">3</a> | <a href="/?p=4">4</a> | <a href="/?p=5">5</a> | <a href="/?p=6">6</a> | <a href="/?p=2">próxima</a>}, paginator(1, 6)
  end

  test "paginator seven pages" do
    assert_equal %{1 | <a href="/?p=2">2</a> | <a href="/?p=3">3</a> | <a href="/?p=4">4</a> | <a href="/?p=5">5</a> | <a href="/?p=6">6</a> | <a href="/?p=7">7</a> | <a href="/?p=2">próxima</a>}, paginator(1, 7)
  end

  test "paginator greater than seven pages" do
    assert_equal %{1 | <a href="/?p=2">2</a> | <a href="/?p=3">3</a> | <a href="/?p=4">4</a> | <a href="/?p=5">5</a> | ... | <a href="/?p=8">8</a> | <a href="/?p=2">próxima</a>}, paginator(1, 8)
    assert_equal %{<a href="/?p=1">anterior</a> | <a href="/?p=1">1</a> | 2 | <a href="/?p=3">3</a> | <a href="/?p=4">4</a> | <a href="/?p=5">5</a> | ... | <a href="/?p=8">8</a> | <a href="/?p=3">próxima</a>}, paginator(2, 8)
    assert_equal %{<a href="/?p=999">anterior</a> | <a href="/?p=1">1</a> | ... | <a href="/?p=999">999</a> | 1000 | <a href="/?p=1001">1001</a> | ... | <a href="/?p=2000">2000</a> | <a href="/?p=1001">próxima</a>}, paginator(1000, 2000)
    assert_equal %{<a href="/?p=1999">anterior</a> | <a href="/?p=1">1</a> | ... | <a href="/?p=1996">1996</a> | <a href="/?p=1997">1997</a> | <a href="/?p=1998">1998</a> | <a href="/?p=1999">1999</a> | 2000}, paginator(2000, 2000)
  end
end
