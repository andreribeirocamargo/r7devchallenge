require 'open-uri'

Mongoid.logger.level = Logger::INFO
Mongo::Logger.logger.level = Logger::INFO

# Use THREADS environment variable to define how many threads you wish to create
#
# To create 10 threads with 4 maximum depth of nested comments do:
#
# $ THREADS=10 DEPTH=4 rake db:seed
#
# THREADS default is 50, DEPTH default is 6
#
max_threads = ENV['THREADS'].to_i
max_threads = 50 if max_threads < 1

max_depth = ENV['DEPTH'].to_i
max_depth = 6 if max_depth < 1

def build_a_word(wordlist, length=5)
  length.times.collect { wordlist[rand(wordlist.length)].chomp }.join(' ')
end

def write_a_comment(wordlist, topic, parent=nil)
  comment = Comment.new
  comment.topic = topic
  comment.parent = parent
  comment.message = build_a_word(wordlist)
  comment.save!
  comment
end

def build_comments(wordlist, topic, deepest, parent=nil, depth=1)
  return if depth > deepest
  batch = rand(1..4)
  progress = ("#" * depth).ljust(deepest)
  puts "Thread %s Depth %02d/%02d Batch %02d %s" % [topic.name, depth, deepest, batch, progress]
  batch.times do
    comment = write_a_comment(wordlist, topic, parent)
    build_comments(wordlist, topic, deepest, comment, depth + 1)
  end
end

def build_a_thread(wordlist, max_depth)
  topic = Topic.new(name: build_a_word(wordlist))
  topic.save!
  puts DateTime.now, "Thread #{topic.name}"
  build_comments(wordlist, topic, rand(1..max_depth))
end


url = "http://alcor.concordia.ca/~vjorge/Palavras-Cruzadas/Lista-de-Palavras.txt"
open(url) do |txt|
  wordlist = txt.readlines
  max_threads.times.each do
    build_a_thread(wordlist, max_depth)
  end
end
