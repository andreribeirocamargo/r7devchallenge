class CommentsController < ApplicationController
  PER_PAGE = 10

  before_filter :load_topic

  caches_action :index,
    cache_path: Proc.new { "comments/#{@topic.id}/#{params[:p].to_i}/#{cache_key(:comments)}" },
    if: Proc.new { flash[:info].blank? && flash[:danger].blank? }

  def index
    @comments_count = @topic.comments_count

    @current_page, @total_pages, @comments = @topic.comments.paginate(
      page: params[:p],
      batch_size: PER_PAGE,
      records_count: @comments_count,
      sorting_algorithm: [{ path: :asc }, { path: :desc }]
    )

    # reorder because of sorting algorithm
    @comments = @comments.all.sort {|a,b| a.path <=> b.path }
  end

  def create
    @comment = @topic.comments.new
    @comment.parent_id = params[:parent_id]
    @comment.message = params[:message]
    if @comment.save
      flash[:info] = "Novo comentário cadastrado!"
      reset_cache_key(:comments, :topics)
    else
      flash[:danger] = "Ops, informe a mensagem do comentário..."
    end

    redirect_to topic_comments_path(@topic, p: @comment.guess_page_in_batch_of(PER_PAGE))
  end

  private

  def load_topic
    redirect_to topics_path unless @topic = Topic.find(params[:topic_id])
  end
end
