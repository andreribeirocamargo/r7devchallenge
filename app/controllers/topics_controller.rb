class TopicsController < ApplicationController
  PER_PAGE = 10

  caches_action :index,
    cache_path: Proc.new { "topics/#{params[:p].to_i}/#{cache_key(:topics)}" },
    if: Proc.new { flash[:info].blank? && flash[:danger].blank? }

  def index
    @topics_count = Topic.count
    @comments_count = Comment.count

    @current_page, @total_pages, @topics = Topic.paginate(
      page: params[:p],
      batch_size: PER_PAGE,
      records_count: @topics_count,
      sorting_algorithm: [{id: :desc}, {id: :asc}]
    )
  end

  def create
    @topic = Topic.new
    @topic.name = params[:name]
    if @topic.save
      flash[:info] = "Novo assunto cadastrado!"
      reset_cache_key(:topics)
    else
      flash[:danger] = "Ops, informe o assunto para iniciar uma discussão..."
    end
    redirect_to topics_path
  end
end
