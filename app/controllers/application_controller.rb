class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_csrf_cookie

  private

  def set_csrf_cookie
    cookies[:r7csrf] = {
      value: form_authenticity_token,
      expires: 1.day.from_now,
      secure: Rails.env.production? # only transmit this cookie over https on production
    }
  end

  def cache_key(name)
    Rails.cache.fetch("#{name}_updated_at") { DateTime.now.to_f }
  end

  def reset_cache_key(*names)
    names.each {|name| Rails.cache.write("#{name}_updated_at", DateTime.now.to_f) }
  end
end
