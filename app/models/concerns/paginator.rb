module Paginator
  extend ActiveSupport::Concern

  module ClassMethods
    def paginate(options={})
      current = options[:page].to_i
      current = 1 if current < 1

      batch_size = options[:batch_size].to_i
      batch_size = 4 if batch_size < 1

      total_records = options[:records_count].to_i

      first_half_sorting, second_half_sorting = options[:sorting_algorithm]

      total_pages = total_records.fdiv(batch_size).ceil
      total_pages = 1 if total_pages < 1

      current = total_pages if current > total_pages

      order = first_half_sorting
      offset = (current - 1) * batch_size
      middle_page = total_pages.fdiv(2).ceil
      if current > middle_page
        order = second_half_sorting
        offset = (total_pages - current) * batch_size
      end
      documents = limit(batch_size).offset(offset).reorder(order)

      [current, total_pages, documents]
    end
  end
end
