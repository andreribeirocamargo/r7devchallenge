module Blacklist
  extend ActiveSupport::Concern

  module ClassMethods
    def blacklist_shuffle(dictionary, head, *tail)
      heads = []
      heads << [head]
      dictionary.each do |char, substitutes|
        heads += substitutes.map {|variation| [variation] } if head == char
      end

      if tail.empty?
        heads
      else
        tails = self.blacklist_shuffle(dictionary, *tail)
        result = []
        heads.each do |new_head|
          tails.each do |new_tail|
            result << (new_head + new_tail)
          end
        end
        result
      end
    end

    def blacklist(words, dictionary)
      words.inject([]) do |accumulator, bad_word_string|
        bad_word_array = bad_word_string.split('')
        bad_word_array_shuffled = self.blacklist_shuffle(dictionary, *bad_word_array)
        regexps = bad_word_array_shuffled.map do |bad_word_shuffled|
          Regexp.new(bad_word_shuffled.join('\s*'), Regexp::IGNORECASE | Regexp::MULTILINE)
        end
        accumulator += regexps
      end
    end
  end
end
