class Topic
  include Paginator
  include Mongoid::Document

  field :created_at, type: DateTime, default: -> { DateTime.now }
  field :name
  field :comments_count, type: Integer, default: -> { 0 }

  validates :name, presence: true

  has_many :comments, dependent: :delete
end
