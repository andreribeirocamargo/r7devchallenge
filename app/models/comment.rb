class Comment
  include Blacklist
  include Paginator
  include Mongoid::Document

  BLACKLIST_WORDS = %w{arma carro}
  BLACKLIST_DICTIONARY = {
    'a' => ['4', '@'],
    'c' => ['k'],
    'o' => ['0']
  }

  field :created_at, type: DateTime, default: -> { DateTime.now }
  field :message
  field :censured_message
  field :path, default: -> { id.to_s }

  index({ path: 1 }, { background: true })

  validates :message, presence: true
  validate :check_parent

  belongs_to :topic, inverse_of: :comments, index: true, counter_cache: :comments_count

  scope :ordered_by_path, -> { asc(:path) }

  def parent_id=(comment_id)
    if comment_id && (comment = Comment.find(comment_id))
      send :parent=, comment
    end
    comment_id
  end

  def parent=(comment)
    new_path = id.to_s
    if comment
      new_path = comment.path + ',' + new_path
    end
    write_attribute :path, new_path
    comment
  end

  def parent
    parent_id && Comment.find(parent_id)
  end

  def parent_id
    path_to_a[-2]
  end

  def check_parent
    if parent && (parent.topic_id != topic_id)
      errors.add :parent, I18n.t('errors.messages.invalid')
    end
  end

  def depth
    path_to_a.length
  end

  def path_to_a
    path.split(',')
  end

  def message=(value)
    write_attribute :censured_message, Comment.replace_bad_words(value)
    write_attribute :message, value
  end

  def guess_page_in_batch_of(batch_size)
    counter = Comment.where(topic_id: topic_id, :path.lte => path).count
    page = counter.fdiv(batch_size)
    page.ceil
  end

  def self.replace_bad_words(string)
    buffer = string.clone
    @@blacklist_cache ||= self.blacklist(BLACKLIST_WORDS, BLACKLIST_DICTIONARY)
    @@blacklist_cache.each do |regexp|
      buffer.gsub!(regexp) do |match|
        if Regexp.last_match.post_match[0] =~ /[a-z]/i
          match
        else
          'x' * match.length
        end
      end
    end
    buffer
  end
end
