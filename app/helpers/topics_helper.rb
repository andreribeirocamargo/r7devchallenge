module TopicsHelper
  def count_comments_for(topic)
    counter = topic.comments_count
    case counter
    when 0
      'nenhum comentário'
    when 1
      '1 comentário'
    else 
      "#{counter} comentários"
    end
  end
end
