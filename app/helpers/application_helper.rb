module ApplicationHelper

  def format_datetime(datetime)
    datetime.strftime "%d/%m/%Y %H:%M:%S"
  end

  def paginator(current_page, total_pages)
    return '' if total_pages < 2

    current_page = total_pages if current_page > total_pages

    delta = total_pages - current_page

    pages = []

    if total_pages > 0
      if current_page > 1
        pages << link_to('anterior', p: current_page - 1)
      end

      # first position
      if current_page == 1
        pages << "1"
      else
        pages << link_to(1, p: 1)
      end

      # second position
      if total_pages > 1
        if total_pages > 7 && current_page > 4
          pages << '...'
        elsif current_page == 2
          pages << '2'
        else
          pages << link_to(2, p: 2)
        end
      end

      # third position
      if total_pages > 2
        if total_pages > 7
          if delta < 4
            pages << link_to(total_pages - 4, p: total_pages - 4)
          else
            if current_page > 4
              pages << link_to(current_page - 1, p: current_page - 1)
            elsif current_page == 3
              pages << "3"
            else
              pages << link_to(3, p: 3)
            end
          end
        else
          if current_page == 3
            pages << "3"
          else
            pages << link_to(3, p: 3)
          end
        end
      end

      # fourth position
      if total_pages > 3
        if total_pages > 7
          if delta <= 3
            page = total_pages - 3
            if page == current_page
              pages << current_page.to_s
            else
              pages << link_to(page, p: page)
            end
          else
            if current_page > 4
              pages << current_page.to_s
            else
              if current_page == 4
                pages << "4"
              else
                pages << link_to(4, p: 4)
              end
            end
          end
        else
          if current_page == 4
            pages << "4"
          else
            pages << link_to(4, p: 4)
          end
        end
      end

      # fifth position
      if total_pages > 4
        if total_pages > 7
          if delta < 3
            page = total_pages - 2
            if page == current_page
              pages << current_page.to_s
            else
              pages << link_to(page, p: page)
            end
          else
            if current_page < 5
              if current_page == 5
                pages << "5"
              else
                pages << link_to(5, p: 5)
              end
            else
              page = current_page + 1
              pages << link_to(page, p: page)
            end
          end
        else
          if current_page == 5
            pages << "5"
          else
            pages << link_to(5, p: 5)
          end
        end
      end

      # sixth position
      if total_pages > 5
        if total_pages > 7
          if delta <= 3
            page = total_pages - 1
            if current_page == page
              pages << current_page.to_s
            else
              pages << link_to(page, p: page)
            end
          else
            pages << '...'
          end
        else
          if current_page == 6
            pages << "6"
          else
            pages << link_to(6, p: 6)
          end
        end
      end

      # seventh position
      if total_pages > 6
        if total_pages > 7
          if current_page == total_pages
            pages << current_page.to_s
          else
            pages << link_to(total_pages, p: total_pages)
          end
        else
          if current_page == 7
            pages << "7"
          else
            pages << link_to(7, p: 7)
          end
        end
      end

      if current_page + 1 <= total_pages
        pages << link_to('próxima', p: current_page + 1)
      end
    end

    raw pages.join(' | ')
  end
end
