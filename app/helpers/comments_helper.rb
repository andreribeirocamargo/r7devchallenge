module CommentsHelper
  def render_comments_tree(topic, comments)
    html = ""
    comments.each do |comment|
      html << render(partial: 'comment', locals: { topic: topic, comment: comment })
    end
    raw html
  end
end
