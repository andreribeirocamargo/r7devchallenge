# R7 Challenge

  This is a forum app prototype builded with performance in mind for R7.com recruitment process.

## How to run it locally

  * Install and load up MongoDB

    Follow the instructions on https://docs.mongodb.com/manual/installation/

  * If you're going to run on production, install and load up Memcached

    Follow the instructions on https://memcached.org/downloads

  * Install RVM

    Follow the instructions on https://rvm.io/rvm/install

  * Install Ruby 2.3.1

    ```
    $ rvm install 2.3.1
    ```

  * Install Bundler gem

    ```
    $ gem install bundler
    ```

  * Install Git

    Follow the instructions on https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

  * Clone this git repository

    ```
    $ git clone https://andreribeirocamargo@bitbucket.org/andreribeirocamargo/r7devchallenge.git
    ```

  * Install the gems required by the application

    ```
    $ cd r7devchallenge
    ```

    ```
    $ bundle install
    ```

  * Initialize the database and create the indexes

    ```
    $ rake db:mongoid:create_indexes
    ```

  * Seed some data to database

    ```
    $ rake db:seed THREADS=1000 DEPTH=2
    ```

  * Run the tests

    ```
    $ rake
    ```

  * Start the web server

    ```
    $ unicorn
    ```

  * Open your browser at http://localhost:8080

## Troubleshooting

  * Check MongoDB configuration at config/mongoid.yml

  * Check Memcached configuration at config/environments/production.rb
