Rails.application.routes.draw do
  resources :topics, path: '/', only: [:index, :create] do
    resources :comments, only: [:index, :create]
  end
end
